#!/bin/bash
{
entorno=expl
[ -z "$JAVA_HOME" ] && echo "Define JAVA_HOME variable" && exit 1
[ "$PATH" == *"$JAVA_HOME/bin"* ] || export PATH=${JAVA_HOME}/bin:$PATH
JVM_ARGS="-Xms256m -Xmx768m $USER_JVM_ARGS"
scriptName=""
export BASE_DIR=$(pwd)
export LANG='en_US.UTF-8'
export NLS_LANG='American_America.AL32UTF8'

rm -f *.log
find ./ -name '*py.class' | xargs rm -f
find ./ -name '*.pyc' | xargs rm -f
#rm -f log/*.lo


java ${JVM_ARGS} -cp lib/jython.jar:lib/log4j-1.2.17.jar:lib/ojdbc7.jar \
         -Dpython.cachedir.skip=false org.python.util.jython cleanup_cmd.py "$@"

}
