#!/bin/bash
 
entorno=inte
ruta_base=/${entorno}/sistemas/scripts/dbCleanup
days_keep=30
days_advance_partitions=7
. ${ruta_base}/logging.sh

SCRIPTENTRY

export JAVA_HOME=/usr/java7_64

cd ${ruta_base}

########################  make new partitions  ######################
./run_new_partitions.sh env=$entorno schema=bo table=app_data days=$days_advance_partitions
./run_new_partitions.sh env=$entorno schema=bo table=app_application days=$days_advance_partitions
./run_new_partitions.sh env=$entorno schema=bo table=aup_tag_value days=$days_advance_partitions
./run_new_partitions.sh env=$entorno schema=bo table=evt_event_object days=$days_advance_partitions
./run_new_partitions.sh env=$entorno schema=bo table=cst_stl_oper_lote days=$days_advance_partitions
./run_new_partitions.sh env=$entorno schema=bo table=opr_operation days=$days_advance_partitions
./run_new_partitions.sh env=$entorno schema=bo table=opr_participant days=$days_advance_partitions
./run_new_partitions.sh env=$entorno schema=bo table=opr_additional_amount days=$days_advance_partitions
./run_new_partitions.sh env=$entorno schema=bo table=opr_card days=$days_advance_partitions

########################  drop old partitions  ######################
./run_cleanup.sh  env=$entorno schema=bo task=app_application_parts        days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=opr_card_parts               days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=app_data_parts               days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=opr_operation_parts          days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=opr_additional_amount_parts  days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=evt_event_object_parts       days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=aup_tag_value_parts          days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=cst_stl_oper_lote_parts      days_keep=$days_keep


########################        cleanup        ######################
######################## price & justificacion ######################
./run_cleanup.sh  env=$entorno schema=bo task=prc_messages_hist            days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=prc_raw_messages_hist        days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=prc_raw_messages             days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=prc_messages_errors          days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=just_prc_raw_messages        days_keep=5 
./run_cleanup.sh  env=$entorno schema=bo task=just_prc_messages            days_keep=40
######################## end of price & justificacion ###############

######################## recon ######################################
./run_cleanup.sh  env=$entorno schema=bo task=recon_sessions_to_delete_create days_keep=$days_keep  
./run_cleanup.sh  env=$entorno schema=bo task=recon_clob_store             days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=recon_price                  days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=recon_ds_counters            days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=recon_te                     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=recon_tsv                    days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=recon_tr                     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=recon_session_audit          days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=recon_concentrator           days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=recon_nps                    days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=recon_tma                    days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=diff_31307                   days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=diff_48230                   days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=diff_68955                   days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=diff_71521                   days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=diff_90618                   days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=recon_case_history           days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=recon_session                days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=recon_sessions_to_delete_drop days_keep=$days_keep
######################## end of recon ###############################

######################## schema=bo #########################################
./run_cleanup.sh  env=$entorno schema=bo task=prc_file_raw_data            days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=cst_stl_oper_session         days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=cst_stl_oper_comp_amount     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=cst_stl_compensation_amount  days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=mcw_text                     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=prc_session                  days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=vis_batch                    days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=vis_file                     days_keep=$days_keep

./run_cleanup.sh  env=$entorno schema=bo task=cup_fin_message              days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=cst_fau_adjustment           days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=cst_rec_exported             days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=opr_oper_stage               days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=opr_oper_fee                 days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=cst_prc_loaded_file          days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=prc_messages                 days_keep=$days_keep


./run_cleanup.sh  env=$entorno schema=bo task=mus_record                   days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=mus_batch                    days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=fibrn_record                 days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=fibrn_batch                  days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=batch_file                   days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=recon_session                days_keep=$days_keep


./run_cleanup.sh  env=$entorno schema=bo task=cst_fau_oper_fees            days_keep=$days_keep

./run_cleanup.sh  env=$entorno schema=bo task=cst_lic_mc_fee               days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=cst_lic_mc_oper              days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=cst_lic_visa_oper            days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=cst_lic_visa_fee             days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=evt_status_log               days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=hermes_alert                 days_keep=$days_keep

./run_cleanup.sh  env=$entorno schema=bo task=cst_rec_decision_operation   days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=vis_vss1                     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=vis_vss2                     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=vis_vss4                     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=vis_vss6                     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=aut_auth                     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=cst_interchange_amount       days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=visa_session_log             days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=visa_calculated_fee          days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=visa_operation               days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=visa_common_operation        days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=mc_operation                 days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=mc_common_operation          days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=mc_calculated_fee            days_keep=$days_keep

./run_cleanup.sh  env=$entorno schema=bo task=cst_stl_pre_date             days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=ntb_note                     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=opr_participant		       days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=mcw_msg_pds                  days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=mcw_add                      days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=mcw_card                     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=mcw_fpd                      days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=mcw_fsum                     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=mcw_spd                      days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=mcw_fin                      days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=mcw_file                     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=vis_card                     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=vis_fin_addendum             days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=vis_fin_message              days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=prc_session_file             days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=cst_stl_lote                 days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=cst_stl_session_calendar     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=prc_session_file_update1     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=prc_session_file_update2     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=prc_session_file_update3     days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=cst_hrm_alert_to_delete_create days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=cst_hrm_alert                days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=cst_hrm_alert_to_delete_drop days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=trc_log                      days_keep=$days_keep
./run_cleanup.sh  env=$entorno schema=bo task=changeset_batch              days_keep=$days_keep
