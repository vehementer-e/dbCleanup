#!/usr/bin/env python
# coding=utf-8
import sys
import time

from dbcleanup.workers import *
from dbcleanup.data_source import *
from custom_log import get_log, log_level
import datetime
import dbcleanup

def cleanup_old_data_cmd(env, schema, task, days_keep = None):
	
	log = get_log('db.cleanup_old_data_cmd', True)
	start_time = time.time()

	log.info('Start cleanup data older than ' + str(days_keep) + ' on env ' + env + ' .. ' + schema + ' .. ' + task)

	ds = allDataSources()[env][schema]
	log.info('ds = ' + str(ds))

	conn = ds.connect()
	
	worker = dbcleanup.create(old_data_workers[task]['worker'], **old_data_workers[task]['args'])
	worker.conn = conn
	if(days_keep):
		worker.days_keep = days_keep

	worker.start()
	worker.join()

	
	if(conn):
		conn.close()


	print(u"Alive time: %s s" % (time.time() - start_time))
	exit(0)




if __name__ == '__main__':

	d = dict(map(lambda x: x.split('='), sys.argv[1:]))
	if('env' in d and 'schema' in d and 'task' in d):
		cleanup_old_data_cmd(**d)