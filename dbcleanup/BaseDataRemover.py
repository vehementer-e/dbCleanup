

from threading import Thread
from custom_log import get_log, log_level
from dbcleanup.db_actions import *

log_find_range = get_log('old_data_remover.find_range', True)

class BaseDataRemover(Thread):

	days_keep = 365
	alive = True
	timeout = 1
	conn = None
	range_query = None
	delete_query = None
	
	def __init__(self):
		Thread.__init__(self)

	def rowid_table_exists(self):
		min, max = self.find_range(self.conn, "select 0 mi, count(1) ma from user_tables where table_name=upper('{rowid_table_name}')".format(rowid_table_name = self.rowid_table_name))
		return max

	def create_rowid_table(self):
		execute(self.conn, self.range_query.format(days_keep = self.days_keep, rowid_table_name = self.rowid_table_name))

	def drop_rowid_table(self):
		execute(self.conn, "drop table {rowid_table_name} purge".format(rowid_table_name = self.rowid_table_name))

	def find_range(self, conn, query):

		curs = None
		min = None
		max = None

		try:
			log_find_range.info("trying to get cursor from conn " + str(conn))
			curs = conn.cursor()
			log_find_range.info('cursor obtained in find_range: ' + str(curs) + "\nexecuting '" + str(query) + "'")
			curs.execute(query)
			if curs.rowcount:
				log_find_range.info("range found")
				min, max = curs.fetchone()
		except Throwable, e:
			log_find_range.info("error in find_range: " + str(e))
		finally:
			if curs:
				curs.close()
		return min, max