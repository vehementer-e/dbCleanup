
from dbcleanup.old_data_remover import *

oper_id_table_name = 'tmp_opr_oid'

partition_range_query = """
							select PARTITION_NAME from (
								select t.PARTITION_NAME, nvl(
									   sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
									   sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
								   ) as highvalue
								from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
								where t.table_name = upper('{table_name}')
									and o.name = t.table_name
									and o.subname = t.partition_name
									and o.obj# = tp.obj#(+)
									and o.obj# = tpp.obj#(+)
							)
							where highvalue <= to_char(sysdate - {days_keep}, 'YYMMDD') || '0000000000'
						"""
partition_drop_query = 'alter table {table_name} drop partition {partition_name} update indexes'


old_data_workers = {
    'opr_participant_parts':
							{
								'worker'	:	"PartitionRangeRemoverTh",
								'args'		:
												{
													'rows_saver'	:	{
																			'class'	:	'RowsSaver', 
																			'args'	:	{
																							'rows_to_save_table'		:	'tmp_opr_part_saved',
																							'rows_to_save_query'		:	'create table {rows_to_save_table} as select * from opr_participant where oper_id in(select id from {oper_id_table_name})',
																							'saved_rows_get_back_query' : """insert into opr_participant
																															(OPER_ID,PARTICIPANT_TYPE,INST_ID,NETWORK_ID,CLIENT_ID_TYPE,CLIENT_ID_VALUE,CUSTOMER_ID,AUTH_CODE,CARD_ID,CARD_INSTANCE_ID,CARD_TYPE_ID,CARD_MASK,CARD_HASH,CARD_SEQ_NUMBER,CARD_EXPIR_DATE,CARD_SERVICE_CODE,CARD_COUNTRY,CARD_NETWORK_ID,CARD_INST_ID,ACCOUNT_ID,ACCOUNT_TYPE,ACCOUNT_NUMBER,ACCOUNT_AMOUNT,ACCOUNT_CURRENCY,MERCHANT_ID,TERMINAL_ID)
																															select
																															OPER_ID,PARTICIPANT_TYPE,INST_ID,NETWORK_ID,CLIENT_ID_TYPE,CLIENT_ID_VALUE,CUSTOMER_ID,AUTH_CODE,CARD_ID,CARD_INSTANCE_ID,CARD_TYPE_ID,CARD_MASK,CARD_HASH,CARD_SEQ_NUMBER,CARD_EXPIR_DATE,CARD_SERVICE_CODE,CARD_COUNTRY,CARD_NETWORK_ID,CARD_INST_ID,ACCOUNT_ID,ACCOUNT_TYPE,ACCOUNT_NUMBER,ACCOUNT_AMOUNT,ACCOUNT_CURRENCY,MERCHANT_ID,TERMINAL_ID
																															from {rows_to_save_table}
																														""",
																							'oper_id_table_name'		:	oper_id_table_name
																						}
																		},
													'range_query'	:	partition_range_query.format(table_name = 'OPR_PARTICIPANT', days_keep = '{days_keep}'),
													'delete_query'	:	partition_drop_query.format(table_name = 'OPR_PARTICIPANT', partition_name = '{partition_name}')
												},
							}
    ,
	'opr_operation_rows_to_save_create':
							{
								'worker'	:	"RowIDTableManagerTh",
								'args'		:
												{
													'ddlQuery'	:	"create table {rowid_table_name} as select rownum rn, id from opr_operation where id < to_number(to_char(sysdate - {days_keep}, 'YYMMDD') || '0000000000') and status in('OPST0500', 'OPST0102', 'OPST0600')",
													'rowid_table_name'	:	oper_id_table_name
												}
							}
    ,	
	'opr_operation_rows_to_save_drop':
							{
								'worker'	:	"RowIDTableManagerTh",
								'args'		:
												{
													'ddlQuery'	:	"drop table {rowid_table_name} purge",
													'rowid_table_name'	:	oper_id_table_name
												}
							}
    ,	
    'prc_file_raw_data':
							{
								'worker'	:	"RowIDRangeDataRemoverTh",
								'args'		:
												{
													'range_query'			:	"create table {rowid_table_name} as select rownum rn, rowid rid from prc_file_raw_data where substr(session_file_id, 1, 6) < to_char(sysdate - 1, 'YYMMDD')",
													'delete_query'			:	'delete from prc_file_raw_data where rowid in (select rid from {rowid_table_name} where rn between {start} and {end})',
													'rowid_table_name'		:	'tmp_prc_frd_rowid',
												}
							}
    ,
    'cst_stl_oper_session':
							{
								'worker'	:	"RowIDRangeDataRemoverTh",
								'args'		:
												{
													'rows_saver'	:	{
																			'class'	:	'RowsSaver', 
																			'args'	:	{
																							'rows_to_save_table'		:	'tmp_csos_saved_{rnd}',
																							'rows_to_save_query'		:	"create table {rows_to_save_table} as select * from cst_stl_oper_session where oper_id in(select id from {oper_id_table_name})",
																							'saved_rows_get_back_query' :	"insert into cst_stl_oper_session select * from {rows_to_save_table}",
																							'oper_id_table_name'		:	oper_id_table_name
																						}
																		},
													'range_query'	:	"create table {rowid_table_name} as select rownum rn, rowid rid from cst_stl_oper_session where oper_id < to_number(to_char(sysdate - {days_keep}, 'YYMMDD') || '0000000000')",
													'delete_query'	:	u'delete from cst_stl_oper_session where rowid in (select rid from {rowid_table_name} where rn between {start} and {end})',
													'rowid_table_name'	:	'tmp_cst_sos_rowid',
													'delete_quant'	:	1000000
												}
							}
    ,
}


