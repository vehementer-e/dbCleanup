
from custom_log import get_log, log_level
from java.lang import Throwable

log_execute = get_log('db_actions.execute', True)
log_frdf = get_log('db_actions.find_rows_dont_fetch', True)
log_find_collection = get_log('old_data_remover.find_collection', True)

def execute(conn, query):

	result = None
	curs = None
	
	try:
		curs = conn.cursor()
		try:
			log_execute.info('executing: %s' % query)
			curs.execute(query)
			conn.commit()
			result = str(curs.rowcount)
			log_execute.info("executed rows :" + result)
		except BaseException, jlx:
			err_msg = str(jlx)
			log_execute.info('jlx = ' + err_msg)
			raise
	except error, e:
			log_execute.info('Error getting cursor :' + str(e))
			raise
	finally:
		if curs:
			curs.close()
		return result


def find_rows(conn, query):

	curs = None
	result = None

	try:
		curs = conn.cursor()
		try:
			log_frdf.info('cursor obtained in find_rows: ' + str(curs) + "\nexecuting '" + str(query) + "'")
			curs.execute(query)
			print('curs executed')
			result = curs.fetchall()
			log_frdf.info('receiver completely received resultset')
		except BaseException, jlx:
			log_execute.info('jlx = ' + str(jlx))
			raise
	except Throwable, e:
		log_frdf.error("Error getting cursor :" + str(e))
		raise
	finally:
		if curs:
			curs.close()
	return result

	
def find_rows_dont_fetch(conn, query, receiver):

	curs = None

	try:
		curs = conn.cursor()
		try:
			log_frdf.info('cursor obtained in find_rows_dont_fetch: ' + str(curs) + "\nexecuting '" + str(query) + "'")
			curs.execute(query)
			print('curs executed')
			receiver.receiveDBResponse(curs)
			log_frdf.info('receiver completely received resultset')
		except BaseException, jlx:
			log_execute.info('jlx = ' + str(jlx))
			raise
	except Throwable, e:
		log_frdf.error("Error getting cursor " + str(e))
		raise
	finally:
		if curs:
			curs.close()
	return curs


def find_value(conn, query):

	curs = None
	result = None

	try:
		log_execute.info("trying to get cursor from conn " + str(conn))
		curs = conn.cursor()
		log_execute.info('cursor obtained in find_collection: ' + str(curs) + "\nexecuting '" + str(query) + "'")
		curs.execute(query)
		log_execute.info("collection found")
		row = curs.fetchone()
		if(row != None):
			result = row[0]
	except Throwable, e:
		log_execute.info("error in find_collection: " + str(e))
		raise
	finally:
		if curs:
			curs.close()
	return result


def find_collection(conn, query):

	curs = None
	rows = None

	try:
		log_find_collection.info("trying to get cursor from conn " + str(conn))
		curs = conn.cursor()
		log_find_collection.info('cursor obtained in find_collection: ' + str(curs) + "\nexecuting '" + str(query) + "'")
		curs.execute(query)
		log_find_collection.info("collection found")
		rows = []
		for row in curs.fetchall():
			rows.append(row)
	except Throwable, e:
		log_find_collection.info("error in find_collection: " + str(e))
		raise
	finally:
		if curs:
			curs.close()
	return rows
