
from com.ziclix.python.sql import zxJDBC
from share.common import *

class OracleTools:


    def __init__(self):
        self.charset = 'UTF8'
        self.driver = u'oracle.jdbc.OracleDriver'


    def getConnection(self, params):
        try:
            conn = zxJDBC.connect(params['db_params']['jdbc_url'], params['bo']['user'], params['bo']['password'], self.driver, autoReconnect="true",
                                  CHARSET=self.charset)
        except zxJDBC.DatabaseError, e:
            pretty_print(u'Database connect error:')
            pretty_print('oshibka .. ' + str(e))
            ping_host()
            sys.exit(1)
        return conn
