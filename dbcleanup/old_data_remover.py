

from dbcleanup.BaseDataRemover import *
from custom_log import get_log, log_level
from threading import Thread
from random import randint
from dbcleanup.db_actions import *
import time
from java.lang import Throwable
from java.lang import Exception


class RowsSaver():

	rows_to_save_table = None
	rows_to_save_query = None
	saved_rows_get_back_query = None
	log = get_log('RowsSaver', True)

	def __init__(self, rows_to_save_table, rows_to_save_query, saved_rows_get_back_query, *args, **kwargs):
	
		self.rows_to_save_table 		= 	rows_to_save_table.format(rnd = str(randint(0, 1000)))
		
		kwargs.update({'rows_to_save_table' : self.rows_to_save_table})
		
		self.rows_to_save_query 		= 	rows_to_save_query.format(**kwargs)
		self.saved_rows_get_back_query 	= 	saved_rows_get_back_query.format(rows_to_save_table = self.rows_to_save_table)


	def save_rows(self, conn):
		self.log.info("save some rows to {0}".format(self.rows_to_save_table))
		execute(conn, self.rows_to_save_query)

	def restore_rows(self, conn):
		self.log.info("get back saved rows")
		execute(conn, self.saved_rows_get_back_query)
		execute(conn, "drop table {0} purge".format(self.rows_to_save_table))

		
class PartitionRangeRemoverTh(BaseDataRemover):

	rows_saver = None
	log = get_log('PartitionRangeRemoverTh', True)
	
	def __init__(self, range_query = None, delete_query = None, conn = None, *args, **kvargs):
		BaseDataRemover.__init__(self)
		self.conn = conn
		self.range_query = range_query
		self.delete_query = delete_query
		if(kvargs['rows_saver']):
			self.rows_saver = kvargs['rows_saver']


	def run(self):
		self.log.info("deleting run")
		
		if(self.rows_saver != None):
			self.rows_saver.save_rows(self.conn)
		
		partitions = find_collection(self.conn, self.range_query.format(days_keep = self.days_keep))
		self.log.info('partitions = ' + str(partitions))
		if (partitions != None and len(partitions) > 0):
			for partition in partitions:
				query = self.delete_query.format(partition_name = partition[0])
				self.log.info("query = {0}".format(query))
				result = execute(self.conn, query)
				time.sleep(self.timeout)

		else:
			self.log.info("no partitions to delete")
		
		if(self.rows_saver != None):
			self.rows_saver.restore_rows(self.conn)


class RowIDRangeDataRemoverTh(BaseDataRemover):

	delete_quant = 300000
	rows_saver = None
	rowid_table_name = 'tmp_default_rowid_table'
	log = get_log('RowIDRangeDataRemoverTh', True)

	def __init__(self, range_query=None, delete_query=None, rowid_table_name = None, delete_quant = None, conn = None, *args, **kwargs):
		BaseDataRemover.__init__(self)
		self.conn = conn
		self.range_query = range_query
		self.delete_query = delete_query
		if(delete_quant):
			self.delete_quant = delete_quant
		if(rowid_table_name):
			self.rowid_table_name = rowid_table_name.format(rnd = str(randint(0, 1000)))
		if(kwargs['rows_saver']):
			self.rows_saver = kwargs['rows_saver']


	def run(self):
		self.log.info("deleting run")

		if(self.rows_saver != None):
			self.rows_saver.save_rows(self.conn)
		
		if(self.rowid_table_exists() == 0):
			self.create_rowid_table()
		min, max = self.find_range(self.conn, "select 1 mi, max(rn) ma from {rowid_table_name}".format(rowid_table_name = self.rowid_table_name))
		self.log.info('min = ' + str(min) + ' ... max = ' + str(max))
		if deletableId(min, max):
			execute(self.conn, "ALTER SESSION ENABLE PARALLEL DML")
			for id in range(long(min), long(max)+1, self.delete_quant):
				self.log.info("min = " + str(id) + " .. max = " + str(id + self.delete_quant))
				self.delete_range(id, id + self.delete_quant)
				self.log.info("executed ")
				time.sleep(self.timeout)
		else:
			self.log.info("no data to delete")
		
		self.drop_rowid_table()
		
		if(self.rows_saver != None):
			self.rows_saver.restore_rows(self.conn)


	def delete_range(self, start, end):
		query = self.delete_query.format(start = str(start), end = str(end), rowid_table_name = self.rowid_table_name)
		self.log.info("query = {0}".format(query))
		execute(self.conn, query)
		

class RowIDTableManagerTh(BaseDataRemover):

	ddlQuery = None
	log = get_log('RowIDTableManagerTh', True)

	def __init__(self, ddlQuery=None, rowid_table_name = None, days_keep = None, conn = None):
		BaseDataRemover.__init__(self)
		self.conn = conn
		self.ddlQuery = ddlQuery
		if(rowid_table_name):
			self.rowid_table_name = rowid_table_name

	def run(self):

		execute(self.conn, self.ddlQuery.format(rowid_table_name = self.rowid_table_name, days_keep = self.days_keep))
		if(self.rowid_table_exists() > 0):
			min, max = find_range(self.conn, "select 1 mi, max(rn) ma from {rowid_table_name}".format(rowid_table_name = self.rowid_table_name))
			self.log.info('min = ' + str(min) + ' ... max = ' + str(max))
		else:
			self.log.info('table ' + self.rowid_table_name + ' does not exist')




def deletableId(min, max):
	return min and max and (int(max) - int(min) >= 0)

