#!/usr/bin/env python
# coding=utf-8
#
from importlib import import_module
from dbcleanup.BaseDataRemover import *


def create(removerClassName, *args, **kwargs):

	try:

		removerModule = __import__('dbcleanup.old_data_remover', fromlist=['blah'])

		removerClass = getattr(removerModule, removerClassName)
		
		new_kwargs = {}
		for k, v in kwargs.items():
			tp = '{0}'.format(type(v))
			print('type({0}) == "{1}"'.format(tp, type(v) == tp))
			if(tp == "<type 'dict'>" and 'class' in v):
				print('creating new instance of {0}'.format(v['class']))
				new_kwargs[k] = create(v['class'], **v['args'])
			else:
				new_kwargs[k] = v
			
#		assert issubclass(removerClass, BaseDataRemover)
		print('remover_class={}'.format(removerClass))

		instance = removerClass(*args, **new_kwargs)

	except (AttributeError, AssertionError):
		raise ImportError('{} is not part of removers collection!'.format(removerClassName))

	return instance