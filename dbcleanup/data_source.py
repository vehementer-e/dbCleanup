import sys
from com.ziclix.python.sql import zxJDBC
from config.data_sources import *
from custom_log import get_log, log_level

log = get_log('db.data_source', True)

class OracleDBDataSource:

	charset = 'UTF8'
	driver = u'oracle.jdbc.OracleDriver'


	def __init__(self, url, user, passwd):
		self.url = url
		self.user = user
		self.passwd = passwd


	def connect(self):

		conn = None

		try:
			log.info(u'Connecting to: %s/*******@//%s' % (self.user, self.url))
			conn = zxJDBC.connect(self.url, self.user, self.passwd, self.driver, autoReconnect="true", CHARSET=self.charset)
			log.info("connected")
		except zxJDBC.DatabaseError, e:
			log.info("error " + str(e))
		except:
			log.info("error " + str(sys.exc_info()[0]))

		finally:
			return conn



class DummyTestDataSource:


	def __init__(self, url, user, passwd):
		self.url = url
		self.user = user
		self.passwd = passwd


	def connect(self):

		try:
			log.info(u'Connecting to: %s/*******@//%s' % (self.user, self.url))
			conn = DummyTestConn()
			log.info("connected")
		except zxJDBC.DatabaseError, e:
			log.info("error " + str(e))
		except:
			log.info("error " + str(sys.exc_info()[0]))

		finally:
			return conn


class DummyTestConn:

	def __init__(self):
		pass
	
	def cursor():
		return DummyTestCursor()

class DummyTestCursor:

	def __init__(self):
		pass
	
	def cursor():
		return 


def allDataSources():

	allDs = {}
	module = __import__('dbcleanup.data_source', fromlist=['blah'])
	
	for env in datasources:
		allDs[env] = {}
		schemas = datasources[env]['schemas']
		for sc in schemas:
			schema = schemas[sc]
			if('ds_class' not in datasources[env]['db_params']):
				ds_class = getattr(module, 'OracleDBDataSource')
			else:
				ds_class = getattr(module, datasources[env]['db_params']['ds_class'])
			allDs[env][sc] = ds_class(datasources[env]['db_params']['jdbc_url'], schema['user'], schema['password'])
	
	return allDs


