
from dbcleanup.old_data_remover import *

tables = {

	##########    aup_tag_value    ############################################    
    'aup_tag_value': {
                        'last_part' : """select PARTITION_NAME from (
														select t.PARTITION_NAME, sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tp.rowid) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tp
														 where t.table_name = 'AUP_TAG_VALUE'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
													)
										where
											highvalue='MAXVALUE'
									"""
						,
                        'max_part_after_sysdate' : """select coalesce(0, to_date(substr(max(highvalue),1,6), 'YYMMDD')-trunc(sysdate)) from (
														select t.PARTITION_NAME, sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tp.rowid) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tp
														 where t.table_name = 'AUP_TAG_VALUE'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
													)
										where
											highvalue<to_char(sysdate+{days_advance}, 'YYMMDD') || '0000000000'
									"""
						,
                        'max_part_before_maxvalue' : """select PARTITION_NAME from (
															select PARTITION_NAME, highvalue from (
																									select t.PARTITION_NAME, sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tp.rowid) as highvalue
																									  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tp
																									 where t.table_name = 'AUP_TAG_VALUE'
																									   and o.name = t.table_name
																									   and o.subname = t.partition_name
																									   and o.obj# = tp.obj#(+)
																								)
																									where
																										highvalue<>'MAXVALUE'
																				order by highvalue desc
															) where rownum=1
									"""
						,
                        'todays_part' 				: """select PARTITION_NAME from (
																					select t.PARTITION_NAME, sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tp.rowid) as highvalue
																					  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tp
																					 where t.table_name = 'AUP_TAG_VALUE'
																					   and o.name = t.table_name
																					   and o.subname = t.partition_name
																					   and o.obj# = tp.obj#(+)
																				)
														where
															highvalue like to_char(sysdate+1, 'YYMMDD') || '%'
													"""
						,
                        'splite_last_part' : """select
													'alter table AUP_TAG_VALUE ' ||
													' split partition {last_part} at (' || to_char(sysdate+{days}, 'YYMMDD') || '0000000000' || ') into' ||
												'(
													partition AUP_TAG_VALUE_' || to_char(sysdate+{days}-1, 'YYMMDD') || ' storage (initial 2M), ' ||
												'	partition AUP_TAG_VALUE_MAXVALUE ' ||
												') update indexes' from dual
									"""
						,
    },
	##########    opr_operation    ############################################    
    'opr_operation': {
                        'last_part' : """select PARTITION_NAME from (
														select t.PARTITION_NAME, sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tp.rowid) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tp
														 where t.table_name = 'OPR_OPERATION'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
													)
										where
											highvalue='MAXVALUE'
									"""
						,
                        'max_part_after_sysdate' : """select coalesce(0, to_date(substr(max(highvalue),1,6), 'YYMMDD')-trunc(sysdate)) from (
														select t.PARTITION_NAME, sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tp.rowid) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tp
														 where t.table_name = 'OPR_OPERATION'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
													)
										where
											highvalue<to_char(sysdate+{days_advance}, 'YYMMDD') || '0000000000'
									"""
						,
                        'max_part_before_maxvalue' : """select PARTITION_NAME from (
																						select PARTITION_NAME, highvalue from (
																									select t.PARTITION_NAME, sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tp.rowid) as highvalue
																									  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tp
																									 where t.table_name = 'OPR_OPERATION'
																									   and o.name = t.table_name
																									   and o.subname = t.partition_name
																									   and o.obj# = tp.obj#(+)
																								)
																								where
																										highvalue<>'MAXVALUE'
																								order by highvalue desc
																					)
															where rownum=1
									"""
						,
                        'todays_part' 				: """select PARTITION_NAME from (
																					select t.PARTITION_NAME, sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tp.rowid) as highvalue
																					  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tp
																					 where t.table_name = 'OPR_OPERATION'
																					   and o.name = t.table_name
																					   and o.subname = t.partition_name
																					   and o.obj# = tp.obj#(+)
																				)
														where
															highvalue like to_char(sysdate+1, 'YYMMDD') || '%'
													"""
						,

                        'splite_last_part' : """select
													'alter table OPR_OPERATION ' ||
													' split partition {last_part} at (' || to_char(sysdate+{days}, 'YYMMDD') || '0000000000' || ') into' ||
												'(
													partition OPR_OPERATION_' || to_char(sysdate+{days}-1, 'YYMMDD') || ' storage (initial 2M), ' ||
												'	partition OPR_OPERATION_MAXVALUE ' ||
												') update indexes' from dual
									"""
						,
    },
	##########    evt_event_object    ############################################    
    'evt_event_object': {
                        'last_part' : """select PARTITION_NAME from (
														select t.PARTITION_NAME, sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tp.rowid) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tp
														 where t.table_name = 'EVT_EVENT_OBJECT'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
													)
										where
											highvalue='MAXVALUE'
									"""
						,
                        'max_part_after_sysdate' : """select coalesce(0, to_date(substr(max(highvalue),1,6), 'YYMMDD')-trunc(sysdate)) from (
														select t.PARTITION_NAME, sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tp.rowid) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tp
														 where t.table_name = 'EVT_EVENT_OBJECT'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
													)
										where
											highvalue<to_char(sysdate+{days_advance}, 'YYMMDD') || '0000000000'
									"""
						,
                        'max_part_before_maxvalue' : """select PARTITION_NAME from (
																						select PARTITION_NAME, highvalue from (
																									select t.PARTITION_NAME, sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tp.rowid) as highvalue
																									  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tp
																									 where t.table_name = 'EVT_EVENT_OBJECT'
																									   and o.name = t.table_name
																									   and o.subname = t.partition_name
																									   and o.obj# = tp.obj#(+)
																								)
																								where
																										highvalue<>'MAXVALUE'
																								order by highvalue desc
																					)
															where rownum=1
									"""
						,
                        'todays_part' 				: """select PARTITION_NAME from (
																					select t.PARTITION_NAME, sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tp.rowid) as highvalue
																					  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tp
																					 where t.table_name = 'EVT_EVENT_OBJECT'
																					   and o.name = t.table_name
																					   and o.subname = t.partition_name
																					   and o.obj# = tp.obj#(+)
																				)
														where
															highvalue like to_char(sysdate+1, 'YYMMDD') || '%'
													"""
						,
                        'splite_last_part' : """select
													'alter table EVT_EVENT_OBJECT ' ||
													' split partition {last_part} at (' || to_char(sysdate+{days}, 'YYMMDD') || '0000000000' || ') into' ||
												'(
													partition EVT_EVENT_OBJECT_' || to_char(sysdate+{days}-1, 'YYMMDD') || ' storage (initial 2M), ' ||
												'	partition EVT_EVENT_OBJECT_MAXVALUE ' ||
												') update indexes' from dual
									"""
						,
    },
	##########    opr_additional_amount    ############################################    
    'opr_additional_amount': {
                        'last_part' : """select PARTITION_NAME from (
														select t.PARTITION_NAME, nvl(
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																					) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
														 where t.table_name = 'OPR_ADDITIONAL_AMOUNT'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
														   and o.obj# = tpp.obj#(+)
													)
										where
											highvalue='MAXVALUE'
									"""
						,
                        'max_part_after_sysdate' : """select coalesce(0, to_date(substr(max(highvalue),1,6), 'YYMMDD')-trunc(sysdate)) from (
														select t.PARTITION_NAME, nvl(
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																					) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
														 where t.table_name = 'OPR_ADDITIONAL_AMOUNT'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
														   and o.obj# = tpp.obj#(+)
													)
										where
											highvalue<to_char(sysdate+{days_advance}, 'YYMMDD') || '0000000000'
									"""
						,
                        'max_part_before_maxvalue' : """select PARTITION_NAME from (
																						select PARTITION_NAME, highvalue from (
																									select t.PARTITION_NAME, nvl(
																																	sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																																	sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																																) as highvalue
																						  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
																						  where t.table_name = 'OPR_ADDITIONAL_AMOUNT'
																						   and o.name = t.table_name
																						   and o.subname = t.partition_name
																						   and o.obj# = tp.obj#(+)
																						   and o.obj# = tpp.obj#(+)
																						)
																						where
																								highvalue<>'MAXVALUE'
																						order by highvalue desc
																					)
															where rownum=1
									"""
						,
                        'todays_part' 				: """select PARTITION_NAME from (
																						select t.PARTITION_NAME, nvl(
																														sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																														sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																													) as highvalue
																						  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
																						  where t.table_name = 'OPR_ADDITIONAL_AMOUNT'
																						   and o.name = t.table_name
																						   and o.subname = t.partition_name
																						   and o.obj# = tp.obj#(+)
																						   and o.obj# = tpp.obj#(+)
																					)
														where
															highvalue like to_char(sysdate+1, 'YYMMDD') || '%'
													"""
						,
                        'splite_last_part' : """select
													'alter table OPR_ADDITIONAL_AMOUNT ' ||
													' split partition {last_part} at (' || to_char(sysdate+{days}, 'YYMMDD') || '0000000000' || ') into' ||
												'(
													partition OPR_ADDITIONAL_AMOUNT_' || to_char(sysdate+{days}-1, 'YYMMDD') || ' storage (initial 2M), ' ||
												'	partition {last_part} ' ||
												') update indexes' from dual
									"""
						,
    },
	##########    app_application    ############################################    
    'app_application': {
                        'last_part' : """select PARTITION_NAME from (
														select t.PARTITION_NAME, nvl(
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																					) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
														 where t.table_name = 'APP_APPLICATION'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
														   and o.obj# = tpp.obj#(+)
													)
										where
											highvalue='MAXVALUE'
									"""
						,
                        'max_part_after_sysdate' : """select coalesce(0, to_date(substr(max(highvalue),1,6), 'YYMMDD')-trunc(sysdate)) from (
														select t.PARTITION_NAME, nvl(
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																					) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
														 where t.table_name = 'APP_APPLICATION'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
														   and o.obj# = tpp.obj#(+)
													)
										where
											highvalue<to_char(sysdate+{days_advance}, 'YYMMDD') || '0000000000'
									"""
						,
                        'max_part_before_maxvalue' : """select PARTITION_NAME from (
																						select PARTITION_NAME, highvalue from (
																									select t.PARTITION_NAME, nvl(
																																	sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																																	sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																																) as highvalue
																						  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
																						  where t.table_name = 'APP_APPLICATION'
																						   and o.name = t.table_name
																						   and o.subname = t.partition_name
																						   and o.obj# = tp.obj#(+)
																						   and o.obj# = tpp.obj#(+)
																						)
																						where
																								highvalue<>'MAXVALUE'
																						order by highvalue desc
																					)
															where rownum=1
									"""
						,
                        'todays_part' 				: """select PARTITION_NAME from (
																						select t.PARTITION_NAME, nvl(
																														sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																														sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																													) as highvalue
																						  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
																						  where t.table_name = 'APP_APPLICATION'
																						   and o.name = t.table_name
																						   and o.subname = t.partition_name
																						   and o.obj# = tp.obj#(+)
																						   and o.obj# = tpp.obj#(+)
																					)
														where
															highvalue like to_char(sysdate+1, 'YYMMDD') || '%'
													"""
						,
                        'splite_last_part' : """select
													'alter table APP_APPLICATION ' ||
													' split partition {last_part} at (' || to_char(sysdate+{days}, 'YYMMDD') || '0000000000' || ') into' ||
												'(
													partition APP_APPLICATION_' || to_char(sysdate+{days}-1, 'YYMMDD') || ' storage (initial 2M), ' ||
												'	partition APP_APPLICATION_MAXVALUE ' ||
												') update indexes' from dual
									"""
						,
    },
	##########    app_data    ############################################    
    'app_data': {
                        'last_part' : """select PARTITION_NAME from (
														select t.PARTITION_NAME, nvl(
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																					) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
														 where t.table_name = 'APP_DATA'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
														   and o.obj# = tpp.obj#(+)
													)
										where
											highvalue='MAXVALUE'
									"""
						,
                        'max_part_after_sysdate' : """select coalesce(0, to_date(substr(max(highvalue),1,6), 'YYMMDD')-trunc(sysdate)) from (
														select t.PARTITION_NAME, nvl(
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																					) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
														 where t.table_name = 'APP_DATA'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
														   and o.obj# = tpp.obj#(+)
													)
										where
											highvalue<to_char(sysdate+{days_advance}, 'YYMMDD') || '0000000000'
									"""
						,
                        'splite_last_part' : """select
													'alter table app_data ' ||
													' split partition {last_part} at (' || to_char(sysdate+{days}, 'YYMMDD') || '0000000000' || ') into' ||
												'(
													partition app_data_' || to_char(sysdate+{days}-1, 'YYMMDD') || ' storage (initial 2M), ' ||
												'	partition {last_part} ' ||
												') update indexes' from dual
									"""
						,
    },
	##########    opr_participant    ############################################    
    'opr_participant': {
                        'last_part' : """select PARTITION_NAME from (
														select t.PARTITION_NAME, sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tp.rowid) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tp
														 where t.table_name = 'OPR_PARTICIPANT'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
													)
										where
											highvalue='MAXVALUE'
									"""
						,
                        'max_part_after_sysdate' : """select coalesce(0, to_date(substr(max(highvalue),1,6), 'YYMMDD')-trunc(sysdate)) from (
														select t.PARTITION_NAME, sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tp.rowid) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tp
														 where t.table_name = 'OPR_PARTICIPANT'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
													)
										where
											highvalue<to_char(sysdate+{days_advance}, 'YYMMDD') || '0000000000'
									"""
						,
                        'max_part_before_maxvalue' : """select PARTITION_NAME from (
																						select PARTITION_NAME, highvalue from (
																									select t.PARTITION_NAME, nvl(
																																	sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																																	sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																																) as highvalue
																						  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
																						  where t.table_name = 'OPR_PARTICIPANT'
																						   and o.name = t.table_name
																						   and o.subname = t.partition_name
																						   and o.obj# = tp.obj#(+)
																						   and o.obj# = tpp.obj#(+)
																						)
																						where
																								highvalue<>'MAXVALUE'
																						order by highvalue desc
																					)
															where rownum=1
									"""
						,
                        'todays_part' 				: """select PARTITION_NAME from (
																						select t.PARTITION_NAME, nvl(
																														sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																														sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																													) as highvalue
																						  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
																						  where t.table_name = 'OPR_PARTICIPANT'
																						   and o.name = t.table_name
																						   and o.subname = t.partition_name
																						   and o.obj# = tp.obj#(+)
																						   and o.obj# = tpp.obj#(+)
																					)
														where
															highvalue like to_char(sysdate+1, 'YYMMDD') || '%'
													"""
						,
                        'splite_last_part' : """select
													'alter table OPR_PARTICIPANT ' ||
													' split partition {last_part} at (' || to_char(sysdate+{days}, 'YYMMDD') || '0000000000' || ') into' ||
												'(
													partition OPR_PARTICIPANT_' || to_char(sysdate+{days}-1, 'YYMMDD') || ' storage (initial 2M), ' ||
												'	partition OPR_PARTICIPANT_MAXVALUE ' ||
												') update indexes' from dual
									"""
						,
    },
	##########    cst_stl_oper_lote    ############################################    
    'cst_stl_oper_lote': {
                        'last_part' : """select PARTITION_NAME from (
														select t.PARTITION_NAME, nvl(
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																					) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
														 where t.table_name = 'CST_STL_OPER_LOTE'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
														   and o.obj# = tpp.obj#(+)
													)
										where
											highvalue='MAXVALUE'
									"""
						,
                        'max_part_after_sysdate' : """select coalesce(0, to_date(substr(max(highvalue),1,6), 'YYMMDD')-trunc(sysdate)) from (
														select t.PARTITION_NAME, nvl(
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																					) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
														 where t.table_name = 'CST_STL_OPER_LOTE'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
														   and o.obj# = tpp.obj#(+)
													)
										where
											highvalue<to_char(sysdate+{days_advance}, 'YYMMDD') || '0000000000'
									"""
						,
                        'max_part_before_maxvalue' : """select PARTITION_NAME from (
																						select PARTITION_NAME, highvalue from (
																									select t.PARTITION_NAME, nvl(
																																	sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																																	sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																																) as highvalue
																						  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
																						  where t.table_name = 'CST_STL_OPER_LOTE'
																						   and o.name = t.table_name
																						   and o.subname = t.partition_name
																						   and o.obj# = tp.obj#(+)
																						   and o.obj# = tpp.obj#(+)
																						)
																						where
																								highvalue<>'MAXVALUE'
																						order by highvalue desc
																					)
															where rownum=1
									"""
						,
                        'todays_part' 				: """select PARTITION_NAME from (
																						select t.PARTITION_NAME, nvl(
																														sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																														sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																													) as highvalue
																						  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
																						  where t.table_name = 'CST_STL_OPER_LOTE'
																						   and o.name = t.table_name
																						   and o.subname = t.partition_name
																						   and o.obj# = tp.obj#(+)
																						   and o.obj# = tpp.obj#(+)
																					)
														where
															highvalue like to_char(sysdate+1, 'YYMMDD') || '%'
													"""
						,
                        'splite_last_part' : """select
													'alter table CST_STL_OPER_LOTE ' ||
													' split partition {last_part} at (' || to_char(sysdate+{days}, 'YYMMDD') || '0000000000' || ') into' ||
												'(
													partition OPER_LOTE_' || to_char(sysdate+{days}-1, 'YYMMDD') || ' storage (initial 2M), ' ||
												'	partition {last_part} ' ||
												') update indexes' from dual
									"""
						,
    },
	##########    opr_card    ############################################    
    'opr_card': {
                        'last_part' : """select PARTITION_NAME from (
														select t.PARTITION_NAME, nvl(
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																					) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
														 where t.table_name = 'OPR_CARD'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
														   and o.obj# = tpp.obj#(+)
													)
										where
											highvalue='MAXVALUE'
									"""
						,
                        'max_part_after_sysdate' : """select coalesce(0, to_date(substr(max(highvalue),1,6), 'YYMMDD')-trunc(sysdate)) from (
														select t.PARTITION_NAME, nvl(
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																						sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																					) as highvalue
														  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
														 where t.table_name = 'OPR_CARD'
														   and o.name = t.table_name
														   and o.subname = t.partition_name
														   and o.obj# = tp.obj#(+)
														   and o.obj# = tpp.obj#(+)
													)
										where
											highvalue<to_char(sysdate+{days_advance}, 'YYMMDD') || '0000000000'
									"""
						,
                        'max_part_before_maxvalue' : """select PARTITION_NAME from (
																						select PARTITION_NAME, highvalue from (
																									select t.PARTITION_NAME, nvl(
																																	sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																																	sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																																) as highvalue
																						  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
																						  where t.table_name = 'OPR_CARD'
																						   and o.name = t.table_name
																						   and o.subname = t.partition_name
																						   and o.obj# = tp.obj#(+)
																						   and o.obj# = tpp.obj#(+)
																						)
																						where
																								highvalue<>'MAXVALUE'
																						order by highvalue desc
																					)
															where rownum=1
									"""
						,
                        'todays_part' 				: """select PARTITION_NAME from (
																						select t.PARTITION_NAME, nvl(
																														sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABPART$', 'HIBOUNDVAL', tp.rowid),
																														sys.dbms_metadata_util.long2varchar(4000, 'SYS.TABCOMPART$', 'HIBOUNDVAL', tpp.rowid)
																													) as highvalue
																						  from all_tab_partitions t, sys.obj$ o, sys.tabcompart$ tpp, sys.tabpart$ tp
																						  where t.table_name = 'OPR_CARD'
																						   and o.name = t.table_name
																						   and o.subname = t.partition_name
																						   and o.obj# = tp.obj#(+)
																						   and o.obj# = tpp.obj#(+)
																					)
														where
															highvalue like to_char(sysdate+1, 'YYMMDD') || '%'
													"""
						,
                        'splite_last_part' : """select
													'alter table OPR_CARD ' ||
													' split partition {last_part} at (' || to_char(sysdate+{days}, 'YYMMDD') || '0000000000' || ') into' ||
												'(
													partition OPR_CARD_' || to_char(sysdate+{days}-1, 'YYMMDD') || ' storage (initial 2M), ' ||
												'	partition {last_part} ' ||
												') update indexes' from dual
									"""
						,
    },
}