#!/usr/bin/env python
# coding=utf-8

from dbcleanup.db_actions import *
from dbcleanup.partitioned_tables import tables


class Statistics():

	conn = None
	table = None
	partition = None
	granularity = ['PARTITION']

	def __init__(self, conn=None, **kwargs):
	
		self.conn = conn
		self.table = kwargs['table']
		self.queries = tables[self.table]
		if 'partition' in kwargs:
			self.partition = kwargs['partition']
		if 'granularity' in kwargs:
			self.granularity = kwargs['granularity'].split(",")


	def gatherStats(self):
	
		if(self.partition == 'new'):
			lastPartitionBeforeMax = self.getLastPartitionBeforeMax()
			max = self.getMaxValuePartition()
			print('last_partition of {0} = {1}'.format(self.table, lastPartitionBeforeMax))
			self.gatherPartStats(part_name=max)
			self.gatherPartStats(part_name=lastPartitionBeforeMax)
		if(self.partition == 'today'):
			todays_part = self.getTodaysPart()
			print('today_part of {0} = {1}'.format(self.table, todays_part))
			self.gatherPartStats(part_name=todays_part)
		elif(self.partition != None):
			print('part of {0} = {1}'.format(self.table, self.partition))
			self.gatherPartStats(part_name=self.partition)



	def gatherPartStats(self, part_name=None):
	
		for i in self.granularity:
			statsQuery = self.prepareStatsQuery(part_name=part_name, granularity=i)
			print('statsQuery={}'.format(statsQuery))
			execute(self.conn, statsQuery)


	def getTodaysPart(self):

		return find_value(self.conn, self.queries['todays_part'])

		
	def getLastPartitionBeforeMax(self):

		return find_value(self.conn, self.queries['max_part_before_maxvalue'])

		
	def getMaxValuePartition(self):

		return find_value(self.conn, self.queries['last_part'])


	def prepareStatsQuery(self, part_name=None, granularity='PARTITION'):

		statsQuery = """
						begin
							dbms_stats.gather_table_stats(OWNNAME=>'MAIN', TABNAME=>'{table}', granularity=>'{granularity}', PARTNAME=>'{part_name}', METHOD_OPT=> 'FOR ALL LOCAL INDEXES', DEGREE=> 8, CASCADE=> TRUE, no_invalidate => false);
						end;
					"""
		return statsQuery.format(table=self.table.upper(), granularity=granularity, part_name=part_name)
