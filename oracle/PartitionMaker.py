#!/usr/bin/env python
# coding=utf-8

from dbcleanup.db_actions import *
from dbcleanup.partitioned_tables import tables

class PartitionMaker():

	conn = None
	days_advance = 7
	table = None
	lastPartition = None
	

	def __init__(self, conn=None, **kwargs):
	
		self.conn = conn
		self.table = kwargs['table']
		self.queries = tables[self.table]
		if 'days_advance' in kwargs:
			self.days_advance = int(kwargs['days_advance'])


	def makeNewPartitions(self):
	
		self.lastPartition = self.getLastPartition()
		print('last_partition of {0} = {1}'.format(self.table, self.lastPartition))
		high = self.max_existing_after_sysdate()
		print('high={}'.format(high))
		for i in range(high+1, self.days_advance+2):
			newPartQuery = find_value(self.conn, self.queries['splite_last_part'].format(days=i, last_part=self.lastPartition))
			print('newPartQuery={}'.format(newPartQuery))
			execute(self.conn, newPartQuery)


	def getLastPartition(self):

		get_lastpart_q = self.queries['last_part']
		return find_value(self.conn, get_lastpart_q)


	def max_existing_after_sysdate(self):
		max = int(find_value(self.conn, self.queries['max_part_after_sysdate'].format(days_advance=self.days_advance)))
		return max if max>=0 else 0