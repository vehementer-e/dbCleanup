import thread
import os

from org.apache.log4j import Logger, PatternLayout, Level, PropertyConfigurator, FileAppender

PropertyConfigurator.configure('config/log4j.properties')

log_level = {
    'off': 0,
    'fatal': 10,
    'error': 20,
    'warn': 30,
    'progress': 35,
    'info': 40,
    'debug': 50
}

log_dir = '/inte/logs/dbCleanup'


def get_log(name=None, threaded=False, level='info'):
    fa = FileAppender()
    fa.setName("FileLogger")
    if threaded:
        thrd_id = str(thread.get_ident())
        logger_name = name + thrd_id
        fa.setFile('%s/%s_thrd_%s.log' % (log_dir, name, thrd_id))
    else:
        logger_name = name
        fa.setFile('%s/%s.log' % (log_dir, name))
    fa.setLayout(PatternLayout("%d{yyyy-MM-dd HH:mm:ss} | %-8.8p | [%c] %m%n"))
    print("LOOOGING GET_LOG: dir='" + log_dir + "'\tname='" + name + "'")
    if 'debug' in level:
        fa.setThreshold(Level.DEBUG)
    elif 'error' in level:
        fa.setThreshold(Level.ERROR)
    else:
        fa.setThreshold(Level.INFO)
    fa.setAppend(True)
    fa.activateOptions()
    nlog = Logger.getLogger(logger_name)
    nlog.addAppender(fa)
    return nlog


if __name__ == 'main':
    # loggingTest is just a string that identifies this log.
    log = get_log(u"loggingTest", True)
    # use the config data in the properties file
    log.info(u'This is the start of the log file')

if not os.path.isdir(log_dir):
    if os.path.exists(log_dir):
        os.remove(log_dir)
    os.makedirs(log_dir)
