#!/usr/bin/env python
# coding=utf-8

from oracle.PartitionMaker import *
from dbcleanup.data_source import *

def getConnection(env, schema='bo', **kwargs):
	ds = allDataSources()[env][schema]
	print('ds = ' + str(ds))
	conn = ds.connect()
	print("conn = " + str(conn))
	return conn



if __name__ == '__main__':

	d = dict(map(lambda x: x.split('='), sys.argv[1:]))
	if('env' not in d):
		print('usage: ./run.sh new_partitions_cmd env=prod [schema=bo table=aup_tag_value days=7]')
		exit(1)
	else:
		conn = getConnection(**d)
		PartitionMaker(conn, **d).makeNewPartitions()
		print('end')