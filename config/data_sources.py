#!/usr/bin/env python
# coding=utf-8



datasources = {
				'inte' : {
							'schemas': {
											'bo': {   
													'user': 'main',
													'password': 'main1',
											},
											'hermes': {   
													'user': 'hermes',
													'password': 'hermes1',
											},
							},
							'db_params': {
										 'jdbc_url': 'jdbc:oracle:thin:@10.69.0.32:1521:svi',
							},
				},

				'esdev' : {
							'schemas': {
											'bo': {   
													'user': 'main_red',
													'password': 'main_red1',
											},
							},
							'db_params': {
										 'jdbc_url': 'jdbc:oracle:thin:@10.129.3.230:1521:sv',
							},
				},

				'config' : {
							'schemas': {
											'bo': {   
													'user': 'main_dev',
													'password': 'main_dev1',
											},
							},
							'db_params': {
										 'jdbc_url': 'jdbc:oracle:thin:@10.7.32.157:1521:sv',
							},
				},

				'test' : {
							'schemas': {
											'bo': {   
													'user': 'main_test',
													'password': 'main_test1',
											},
							},
							'db_params': {
										 'jdbc_url': 'jdbc:oracle:thin:@10.7.32.216:1521:sv',
							},
				},

				'dummy_test' : {
							'schemas': {
											'bo': {   
													'user': 'main_red1',
													'password': 'main_red1',
											},
							},
							'db_params': {
										 'jdbc_url': 'jdbc:oracle:thin:@10.129.3.230:1521:sv',
										 'ds_class': 'DummyTestDataSource',
							},
				},

}